#!/usr/bin/ruby

require 'discordrb'
require 'yaml'

VERSION = 'Beta'.freeze # This will be used later
config = YAML.load_file('config.yaml')
bot = Discordrb::Commands::CommandBot.new token: config['token'], client_id: 309_874_706_155_044_874, prefix: ','

puts "\n Nova has started! Invite URL: #{bot.invite_url}&permissions=8194 "
responces = ['Done. And about time too.', 'Bye bye', 'Exterminated.', 'Get the heck of of Dodge!']

bot.command(:roll, max_args: 1, description: 'Rolls a 6 sided die, or a specified number of sides.', useage: 'roll <sides>') do |_event, sides|
  if !sides.nil?
    rand(0..sides.to_i)
  else
    rand(1..6)
  end
end

bot.command(:ping, description: 'Displays the ping time of the bot based on the system\'s time and timestamp of the command.') do |event|
  m = event.respond('Pong!')
  ping = (Time.now - event.timestamp)
  m.edit "Pong! Time taken: #{ping} seconds."
end

bot.command(:shutdown, help_available: false) do |event|
  break unless event.user.id == config['owner']

  bot.send_message(event.channel.id, 'Nova is shutting down')
  exit
end

bot.command(:delete, description: 'Mass delete messages in this channel.',
                     min_args: 1, required_permissions: [:manage_messages],
                     usage: 'delete <ammount>') do |event, ammount|
  if event.bot.profile.on(event.server).permission? :manage_messages
    ammount = ammount.to_i
    next "Can't delete less than 2 messages." if ammount < 2

    while ammount > 100
      event.channel.prune(100)
      ammount -= 100
    end
    event.channel.prune(ammount) if ammount >= 2
    nil
  end
end

bot.command(:about, description: 'Displays basic information about the bot.') do |event|
  event << "#{bot.profile.username}, running the BSD licenced Novarb bot script version #{VERSION}."
  event << 'Source: https://gitlab.com/TheOtherUnknown/Novarb '
  event << "Owner ID: #{config['owner']}"
end

bot.command(:kick, description: 'Kick a user from the server.', min_args: 1, required_permissions: [:kick_members], usage: 'kick <name>') do |event|
  if event.bot.profile.on(event.server).permission? :kick_members
    mentions = event.message.mentions
    event.server.kick(mentions.first)
    unless mentions.first.nil? responces[rand(1..mentions.length) - 1]

    end
  end
end

bot.command(:rehash, description: 'Reloads the configuration file without restarting.', required_permissions: [:administrator], usage: 'rehash') do |_event|
  config = YAML.load_file('config.yaml')
  'Done.'
end

bot.command(:debug, description: 'Shows debugging info about the currently running system.', usage: 'debug') do |event|
  net = system('ping -c 3 discordapp.com').to_s
  event << "```Networking = #{net}"
  event << "Ruby version = #{RUBY_VERSION}"
  event << "Bot version = #{VERSION}"
  event << "Owner ID = #{config['owner']}```"
end

# End commands, start events

event.member_join do |event|
  "#{event.user.mention} has joined the server."
end
bot.run
