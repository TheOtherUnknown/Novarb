#!/bin/bash 

which ruby
if [ $? != 0]; then
  echo "Ruby was not found!"
  exit 1

gem which discordrb
elif [ $? != 0 ]; then
  echo "Ruby Gems or discordrb not found!"
  exit 1
fi

if [ ! -s "./config.yaml"]; then
  truncate -s 0 "./config.yaml" 
fi

echo "Please input the token to be used for your bot: "
read token 
echo "token: " $token >> "./config.yaml" 
echo "Please input the user ID of the bot owner: " 
read userid
echo "owner: " $userid >> "./config.yaml" 
echo "Install sucessful! Run ruby novabot.rb to start the bot."